### ESParam
Common boiler plate code for using the Param library with the ESP32. 
- Param library integration
    - save / load from flash
    - websocket for webparam interface 
    - receive OSC over UDP 
- wifi/eth management
- OTA flashing 
- webserver for web param interface
- Oled support with printer class 

### Wifi
On boot the device will first look to connect to the configured access point.
If it connects it should maintain the connection?

If connection fails, the device should open a access point with its device name and maybe a few numbers of the MAC address. Start captive portal to webparam, use a default IP to connect to the device?

### Platformio
upload code `pio run -t upload`
upload code OTA `pio run -t upload --upload-port 10.0.0.55`
upload files `pio run -t uploadfs`
upload without recompile `pio run -t nobuild -t upload`

#### build flags
To customize Esparam use platformio buildflags.
```
build_flags = 
    -D ESPARAM_ENABLE_ETHERNET=0
    -D ESPARAM_ENABLE_OSC=0
    '-D ESPARAM_DEFAULT_WIFI_SSID="my_desired_default_ssid"'
```
