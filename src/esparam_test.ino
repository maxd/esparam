// ESParam example / test
// for a esp32-s2 with an oled screen
// https://github.com/maxd/ESParam


#include "ESParam.h"
#include "ESParamPrinter.h"

#define STATUS_LED_PIN 2

// a simple parameter to control the led
BoolParam ledParam;

EnumParam myEnumTest;
EnumOption myEnumOptions[] = {
    {"one", 1},
    {"two", 32},
    {"three", -32},
    {"four", 4},
    {"five", 5},
    {"six", 6}
};
IntParam enumFeeder;

void setLED(bool _v){
    digitalWrite(STATUS_LED_PIN, _v);
}

void enumInput(int _v){
    printer.printf("%s -> %i\n", myEnumOptions[myEnumTest.index].name, _v);
}

void setup(){
    Serial.begin(115200);
    printer.begin(&Serial);
    pinMode(STATUS_LED_PIN, OUTPUT);
    digitalWrite(STATUS_LED_PIN, HIGH);

    // setup the parameter
    ledParam.set("/device/led", 0);
    ledParam.saveType = SAVE_ON_REQUEST;
    ledParam.setCallback(setLED);
    paramCollector.add(&ledParam);

    myEnumTest.set("/my/enum", 1);
    myEnumTest.setOptions(myEnumOptions, sizeof(myEnumOptions) / sizeof(EnumOption));
    myEnumTest.saveType = SAVE_ON_REQUEST;
    myEnumTest.setCallback(enumInput);
    paramCollector.add(&myEnumTest);

    enumFeeder.set("/my/enum/feeder", 0,255,127);
    paramCollector.add(&enumFeeder);
    // start the printer and esparam
    // this sets up all of ESParam's parameters, loads config from flash
    esparam_setup();
    // after that we can start the network interfaces
    esparam_start_network();

    digitalWrite(STATUS_LED_PIN, LOW);
}


void loop(){
    esparam_update();
    if(enumFeeder.checkFlag()){
        myEnumTest.receiveByte((byte)enumFeeder.v);
    }
}