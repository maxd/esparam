/*
esparam.h
Boiler plate to integrate Param library on the esp32
Features :
- basic set of parameters to configure network interfaces
- param websocket for web interface
- save and load parameters from esp32 flash
- receive OSC
- over the air flashing

@author maxd@nnvtn.ca
*/
#ifndef ESPARAM_H
#define ESPARAM_H

#include <ESParam_conf.h>
#include <WiFi.h>
#include <Param.h>


#define ESPARAM_USE_ETHERNET (ESPARAM_BOARD_HAS_W5500 || ESPARAM_BOARD_HAS_LAN8720A)


#if ESPARAM_ENABLE_OSC
    #include <MicroOsc.h>
    #include <MicroOscUdp.h>
    #include <ParamOsc.h>

    extern MicroOscUdp<ESPARAM_MICROSC_BUFFER_SIZE> myMicroOscUdp;
    extern bool oscInputFlag;
    extern WiFiUDP oscUdpSocket;
    extern IPAddress oscRemoteIP;
#endif // ESPARAM_ENABLE_OSC

// conf struct
typedef struct {
    // wifi params
    BoolParam wifiEnableParam;
    BoolParam wifiDhcpParam;
    TextParam wifiStaticIpParam;
    TextParam wifiGatewayParam;
    TextParam wifiSubnetParam;
    TextParam wifiSsidParam;
    TextParam wifiPswdParam;
    TextParam deviceNameParam;
    BoolParam forceAccessPointParam;
#if ESPARAM_USE_ETHERNET
    // eth params
    BoolParam ethEnableParam;
    BoolParam ethDhcpParam;
    TextParam ethIpParam;
    TextParam ethGatewayParam;
    TextParam ethSubnetParam;
    IPAddress ethIp, ethGateway, ethSubnet;
#endif
    // other params
#if ESPARAM_ENABLE_OSC
    IntParam oscInPortParam;
    IntParam oscOutPortParam;
    TextParam oscOutIpParam;
#endif

} ESParamConf;


// we provide a way to interact with the config
// on boot one may decide to alter some options
extern ESParamConf epConf;

// we provide a global ParamCollector to collect parameters throughout the code
extern ParamCollector paramCollector;
// namespace wt32_params;
void esparam_setup();
void esparam_start_network();
void esparam_update();

void esparam_send_to_weblog(char * _log);
void esparam_websocket_send(char * _msg);

const char * esparam_get_devicename();
const char * esparam_get_ssid();
bool esparam_is_access_point();
String esparam_get_ip();
bool esparam_eth_connected();
bool esparam_wifi_connected();
bool esparam_ota_running();
void esparam_set_osc_fallback(void (*fallbackCallback)(MicroOscMessage&));
#endif